var class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy =
[
    [ "ShotPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy_1_1_shot_pattern.html", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy_1_1_shot_pattern" ],
    [ "GetCurrentPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#ada8e32fa9f05427bb77cf7d7cdbe42e4", null ],
    [ "Initialize", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#a31891c10333661b4f74ed821d086015b", null ],
    [ "OnDeath", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#a8ca9ea5761ad7a4218a4721a9e6494ce", null ],
    [ "canBeDamaged", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#a1be3f96aae5b57442402f2fdc6af94b9", null ],
    [ "currentPhase", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#a12267a093e8ce0031d4cc0be035402fb", null ],
    [ "scoreOnDeath", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#aa4ce879e2ab57be34a71ec525b0b8bdd", null ],
    [ "shotPatterns", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#ac86a71326b6a67d8f485c9eea018562f", null ],
    [ "ScoreOnDeath", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html#ad6f7094a4f9d4f4f914c3c673e3aeb21", null ]
];