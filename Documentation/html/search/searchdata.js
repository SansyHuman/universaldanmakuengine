var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuwxy",
  1: "cipsu",
  2: "s",
  3: "iu",
  4: "abcdefgimnoprstuw",
  5: "abcdefhilmnoprstuxy",
  6: "cmpt",
  7: "bcenpsu",
  8: "bcdehimopst",
  9: "m"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

