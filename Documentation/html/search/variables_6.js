var searchData=
[
  ['hasendtime',['hasEndTime',['../struct_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_movement.html#a49cc4de8165e6c5aa3658ab0314b1ec9',1,'SansyHuman.UDE.UDEBulletMovement.hasEndTime()'],['../class_sansy_human_1_1_u_d_e_1_1_u_d_e_common_bullet_movement_builder.html#aeac2e7737de8aa1ec9c34823bd588018',1,'SansyHuman.UDE.UDECommonBulletMovementBuilder.hasEndTime()']]],
  ['hastimelimit',['hasTimeLimit',['../class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#aa5cd87940168201f74824ba30bb59aa4',1,'SansyHuman::UDE::UDEBaseShotPattern']]],
  ['health',['health',['../class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_character.html#a33192c13e9d7092c793bca1f5d3ab997',1,'SansyHuman.UDE.UDEBaseCharacter.health()'],['../class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy_1_1_shot_pattern.html#ab91048d8cc5db0ef39597559a14cc7cc',1,'SansyHuman.UDE.UDEEnemy.ShotPattern.health()']]]
];
