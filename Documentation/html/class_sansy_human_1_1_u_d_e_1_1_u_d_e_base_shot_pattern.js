var class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern =
[
    [ "PatternType", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a0f76c22374c882dcee1b01530b666b24", [
      [ "Normal", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a0f76c22374c882dcee1b01530b666b24a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "Special", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a0f76c22374c882dcee1b01530b666b24ab4c2b550635fe54fd29f2b64dfaca55d", null ]
    ] ],
    [ "AddBullet", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a0fcc2fff99c883c3ae97608807e487e9", null ],
    [ "Initialize", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#ad9894fbbabfcab3d36ac38e558e3fd6d", null ],
    [ "operator -", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#ac1441b1b5ea9c5c4b5ff6ae38795d831", null ],
    [ "operator+", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a20107f494935e7d5aef425ef1bae6a5e", null ],
    [ "RemoveBullet", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#abaa2a2184987ae9a1c89381c45887e30", null ],
    [ "ResetPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a612321bb52e2d117e66370d89398a33b", null ],
    [ "ShotPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a7c69b7c17db1aa00b66badc6801e9053", null ],
    [ "StartPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#ac2a9f2b17163c81a15985ec862a6019d", null ],
    [ "StopPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#aa358ee5379ee6ed44cf64a9d88923fcb", null ],
    [ "baseBullets", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a8f876ae6f7a77a2bebe5640c828c8cd3", null ],
    [ "hasTimeLimit", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#aa5cd87940168201f74824ba30bb59aa4", null ],
    [ "originEnemy", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#acce801416f18d76a886993f0b4480fda", null ],
    [ "pattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a217ca14a3f7854467af48944f1facbcf", null ],
    [ "shotPatternOn", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a6e71dff1c118b64303f4d808e2f5f20c", null ],
    [ "shottedBullets", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#af6dacfccaab04885b0b3f65bf4f39aba", null ],
    [ "time", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a9b8037959ac8b0f21203d8b4ba2b6568", null ],
    [ "timeLimit", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#aeac154b9ddc646385228ae4aee83beb2", null ],
    [ "type", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a3369f80124cec0cd3727597d09e5e726", null ],
    [ "HasTimeLimit", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#aff907c55dd0c083a862d093c285f1c71", null ],
    [ "Time", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a256d4033475905c3007719a637c31f59", null ],
    [ "TimeLimit", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a18ea6b11fbcb27f8170c894082948191", null ],
    [ "Type", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html#a5c15c6455ec3af6a065aa248980634bd", null ]
];