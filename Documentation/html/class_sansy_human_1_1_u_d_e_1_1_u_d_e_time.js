var class_sansy_human_1_1_u_d_e_1_1_u_d_e_time =
[
    [ "TimeScale", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a7fd630bff65bf98d13451fdb51fd7238", [
      [ "ENEMY", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a7fd630bff65bf98d13451fdb51fd7238a92b09d1635332c90ae8508618a174244", null ],
      [ "PLAYER", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a7fd630bff65bf98d13451fdb51fd7238a07c80e2a355d91402a00d82b1fa13855", null ],
      [ "UNSCALED", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a7fd630bff65bf98d13451fdb51fd7238aeccae2d9e65893450f366f0be631ef72", null ]
    ] ],
    [ "WaitForScaledSeconds", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a0ea7f4139e09f5a1b1f415b7fdad3870", null ],
    [ "EnemyTimeScale", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#a0ec66ea95aa6b99b80201fad19f392eb", null ],
    [ "PlayerTimeScale", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html#abaa9351092236dc82d5be9d5c7419370", null ]
];