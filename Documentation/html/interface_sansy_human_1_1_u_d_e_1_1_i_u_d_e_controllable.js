var interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable =
[
    [ "Move", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html#acd9c5119b9524f8e45acfe27b6548da4", null ],
    [ "ShotBullet", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html#a63dafa5f8db8b4c4dfaff7f586faae3f", null ],
    [ "BulletFireInterval", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html#ae16cdebc8d8d7adae61cc1f70c83a957", null ],
    [ "SlowModeSpeedAmplifier", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html#ad94e93ee283144155bc068ae34facf6f", null ],
    [ "Speed", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html#a7d99cbc2e0b0af2fcf53e4837e2040e6", null ]
];