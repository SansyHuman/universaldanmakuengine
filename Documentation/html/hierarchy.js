var hierarchy =
[
    [ "Exception", null, [
      [ "SansyHuman.UDE.UDEInvalidBulletMovementException", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_invalid_bullet_movement_exception.html", null ],
      [ "SansyHuman.UDE.UDENotBulletException", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_not_bullet_exception.html", null ],
      [ "SansyHuman.UDE.UDEPatternRunningException", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_pattern_running_exception.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "SansyHuman.UDE.UDEMath.CartesianCoord", "struct_sansy_human_1_1_u_d_e_1_1_u_d_e_math_1_1_cartesian_coord.html", null ],
      [ "SansyHuman.UDE.UDEMath.PolarCoord", "struct_sansy_human_1_1_u_d_e_1_1_u_d_e_math_1_1_polar_coord.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDEBulletPool.InitializingObject", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_pool_1_1_initializing_object.html", null ],
    [ "SansyHuman.UDE.IUDEControllable", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_controllable.html", [
      [ "SansyHuman.UDE.UDEPlayer", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_player.html", null ]
    ] ],
    [ "SansyHuman.UDE.IUDELaserFirable", "interface_sansy_human_1_1_u_d_e_1_1_i_u_d_e_laser_firable.html", null ],
    [ "MonoBehaviour", null, [
      [ "SansyHuman.UDE.UDEBaseBullet", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_bullet.html", [
        [ "SansyHuman.UDE.UDEHomingBullet", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_homing_bullet.html", null ]
      ] ],
      [ "SansyHuman.UDE.UDEBaseCharacter", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_character.html", [
        [ "SansyHuman.UDE.UDEEnemy", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy.html", null ],
        [ "SansyHuman.UDE.UDEPlayer", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_player.html", null ]
      ] ],
      [ "SansyHuman.UDE.UDEBaseShotPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_shot_pattern.html", null ],
      [ "SansyHuman.UDE.UDEBaseStagePattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_base_stage_pattern.html", null ],
      [ "SansyHuman.UDE.UDEBulletGroup", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_group.html", null ],
      [ "SansyHuman.UDE.UDECurve", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_curve.html", null ],
      [ "SansyHuman.UDE.UDESingleton< T >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_singleton.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDEEnemy.ShotPattern", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_enemy_1_1_shot_pattern.html", null ],
    [ "SansyHuman.UDE.UDEBulletMovement", "struct_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_movement.html", null ],
    [ "SansyHuman.UDE.UDECommonBulletMovementBuilder< T >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_common_bullet_movement_builder.html", null ],
    [ "SansyHuman.UDE.UDECommonBulletMovementBuilder< UDECartesianMovementBuilder >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_common_bullet_movement_builder.html", [
      [ "SansyHuman.UDE.UDECartesianMovementBuilder", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_cartesian_movement_builder.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDECommonBulletMovementBuilder< UDEPolarMovementBuilder >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_common_bullet_movement_builder.html", [
      [ "SansyHuman.UDE.UDEPolarMovementBuilder", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_polar_movement_builder.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDEMath", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_math.html", null ],
    [ "SansyHuman.UDE.UDESingleton< UDEBulletManager >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_singleton.html", [
      [ "SansyHuman.UDE.UDEBulletManager", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_manager.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDESingleton< UDEBulletPool >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_singleton.html", [
      [ "SansyHuman.UDE.UDEBulletPool", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_bullet_pool.html", null ]
    ] ],
    [ "SansyHuman.UDE.UDESingleton< UDETime >", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_singleton.html", [
      [ "SansyHuman.UDE.UDETime", "class_sansy_human_1_1_u_d_e_1_1_u_d_e_time.html", null ]
    ] ]
];