var class_u_d_e_enemy =
[
    [ "ShotPattern", "class_u_d_e_enemy_1_1_shot_pattern.html", "class_u_d_e_enemy_1_1_shot_pattern" ],
    [ "GetCurrentPattern", "class_u_d_e_enemy.html#a241f0b1bd19676e235d18b51775d8289", null ],
    [ "Initialize", "class_u_d_e_enemy.html#ac175a3f2ad294b78cb1f57865b2cc281", null ],
    [ "OnDeath", "class_u_d_e_enemy.html#a99ede3d737526bcba8dfce7fc07048a1", null ],
    [ "canBeDamaged", "class_u_d_e_enemy.html#a4cc2ee7ea7872d50dc7e4a35163bb78b", null ],
    [ "currentPhase", "class_u_d_e_enemy.html#a8c0e9bece2fc9bad05f47589d381b181", null ],
    [ "scoreOnDeath", "class_u_d_e_enemy.html#a9dbb0de570b4ae9142653e39719601ec", null ],
    [ "shotPatterns", "class_u_d_e_enemy.html#aa0a489197c7b9bbebf0285979f52891c", null ]
];