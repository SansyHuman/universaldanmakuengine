﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using UnityEditor;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Object;

namespace SansyHuman.UDE
{
    [CustomEditor(typeof(UDEBaseShotPattern), true)]
    public class UDEBaseShotPatternEditor : Editor
    {
        SerializedProperty baseBullets;
        SerializedProperty type;
        SerializedProperty hasTimeLimit;
        SerializedProperty timeLimit;

        SerializedProperty shotPatternOn;

        UDEBaseShotPattern targetPattern;

        private void OnEnable()
        {
            targetPattern = (UDEBaseShotPattern)target;

            baseBullets = serializedObject.FindProperty("baseBullets");
            type = serializedObject.FindProperty("type");
            hasTimeLimit = serializedObject.FindProperty("hasTimeLimit");
            timeLimit = serializedObject.FindProperty("timeLimit");

            shotPatternOn = serializedObject.FindProperty("shotPatternOn");
        }

        bool showProperties = true;
        bool showStatus = false;

        GUIContent baseBulletLabel = new GUIContent("Bullets Used in the Pattern");
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            showProperties = EditorGUILayout.Foldout(showProperties, "Properties", true);
            if (showProperties)
            {
                EditorGUILayout.PropertyField(baseBullets, baseBulletLabel);
                if (baseBullets.isExpanded)
                {
                    EditorGUI.indentLevel++;
                    baseBullets.arraySize = EditorGUILayout.IntField("Number of Bullets", baseBullets.arraySize);
                    EditorGUILayout.LabelField("Bullets");
                    EditorGUI.indentLevel++;
                    for (int i = 0; i < baseBullets.arraySize; i++)
                    {
                        SerializedProperty bullet = baseBullets.GetArrayElementAtIndex(i);
                        bullet.objectReferenceValue = ((UDEBaseBullet)EditorGUILayout.ObjectField("Bullet " + i,
                            ((GameObject)bullet.objectReferenceValue).GetComponent<UDEBaseBullet>(), typeof(UDEBaseBullet), false))
                            .gameObject;
                    }
                    EditorGUI.indentLevel -= 2;
                }
                targetPattern.Type = (UDEBaseShotPattern.PatternType)EditorGUILayout.EnumPopup("Pattern Type", targetPattern.Type);
                hasTimeLimit.boolValue = EditorGUILayout.Toggle("Limit the Pattern's Play Time", hasTimeLimit.boolValue);
                if (hasTimeLimit.boolValue)
                    timeLimit.floatValue = EditorGUILayout.Slider("Time Limit", timeLimit.floatValue, 0, 240);
            }
            EditorGUILayout.Space();

            showStatus = EditorGUILayout.Foldout(showStatus, "Status", true);
            if (showStatus)
            {
                EditorGUILayout.LabelField("Status", shotPatternOn.boolValue ? "Playing" : "Stopped");
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}