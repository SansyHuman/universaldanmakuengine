﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Exception;

namespace SansyHuman.UDE.Management
{
    /// <summary>
    /// Object pool for bullets.
    /// </summary>
    [AddComponentMenu("UDE/Object Pool/Bullet Pool")]
    public class UDEBulletPool : UDESingleton<UDEBulletPool>
    {
        /// <summary>
        /// Internal class to represent pooling bullets and their initial number
        /// </summary>
        [Serializable]
        public class InitializingObject
        {
            /// <summary>
            /// A prefab that has a component <see cref="UDEBaseBullet"/> or its children
            /// </summary>
            public GameObject Prefab;
            /// <summary>
            /// Initial number of the bullet
            /// </summary>
            public int InitialNumber;
        }

        [SerializeField]
        private List<InitializingObject> InitializingObjectList = null;

        private Dictionary<GameObject, List<UDEBaseBullet>> PoolList;

        protected override void Awake()
        {
            base.Awake();
            for (int i = 0; i < InitializingObjectList.Count; i++)
            {
                try
                {
                    InitializingObject init = InitializingObjectList[i];
                    if (init.Prefab.GetComponent<UDEBaseBullet>() == null)
                    {
                        string message = "The object in the InitializingObjectList index of " + i + " is not a bullet object.";
                        throw new UDENotBulletException(message)
                        {
                            InvalidObjectName = init.Prefab.name
                        };
                    }
                }
                catch (UDENotBulletException e)
                {
                    Debug.LogException(e);
                    Debug.LogError("Object Name: " + e.InvalidObjectName);
                    InitializingObjectList.RemoveAt(i);
                    i--;
                }
            }
            CreatePools();
        }

        private void CreatePools()
        {
            PoolList = new Dictionary<GameObject, List<UDEBaseBullet>>(InitializingObjectList.Count);
            for (int i = 0; i < InitializingObjectList.Count; i++)
            {
                InitializingObject init = InitializingObjectList[i];
                List<UDEBaseBullet> pool = new List<UDEBaseBullet>(init.InitialNumber);
                for (int j = 0; j < init.InitialNumber; j++)
                {
                    GameObject instance = Instantiate<GameObject>(init.Prefab, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                    instance.SetActive(false);
                    instance.name = init.Prefab.name;
                    instance.transform.parent = this.transform;
                    pool.Add(instance.GetComponent<UDEBaseBullet>());
                }
                PoolList.Add(init.Prefab, pool);
            }
        }

        /// <summary>
        /// Gets <see cref="UDEBaseBullet"/> instance of the target object.
        /// </summary>
        /// <param name="target">Bullet object to get</param>
        /// <returns><see cref="UDEBaseBullet"/> instance</returns>
        /// <exception cref="UDENotBulletException">Thrown when the target object does not contain 
        /// <see cref="UDEBaseBullet"/> or its child script as the object's component</exception>
        public UDEBaseBullet GetBullet(GameObject target)
        {
            if (target.GetComponent<UDEBaseBullet>() == null)
                throw new UDENotBulletException("The target object to get is not a bullet.")
                {
                    InvalidObjectName = target.name
                };

            UDEBaseBullet returnInstance = null;

            try
            {
                List<UDEBaseBullet> targetPool = PoolList[target];
                for (int i = 0; i < targetPool.Count; i++)
                {
                    UDEBaseBullet inst = targetPool[i];
                    if (!inst.gameObject.activeSelf)
                    {
                        inst.gameObject.SetActive(true);
                        returnInstance = inst;
                        break;
                    }
                }
                if (returnInstance == null)
                {
                    GameObject instance = Instantiate<GameObject>(target, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                    instance.name = target.name;
                    instance.transform.parent = this.transform;
                    UDEBaseBullet scriptInstance = instance.GetComponent<UDEBaseBullet>();
                    targetPool.Add(scriptInstance);
                    returnInstance = scriptInstance;
                }

                return returnInstance;
            }
            catch (KeyNotFoundException e)
            {
                Debug.LogException(e);
                Debug.Log("The target bullet is not found. Create new pool for the bullet.");
                List<UDEBaseBullet> pool = new List<UDEBaseBullet>(1);
                GameObject instance = Instantiate<GameObject>(target, new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                instance.name = target.name;
                instance.transform.parent = this.transform;
                UDEBaseBullet scriptInstance = instance.GetComponent<UDEBaseBullet>();
                pool.Add(scriptInstance);
                this.PoolList.Add(target, pool);
                returnInstance = scriptInstance;

                return returnInstance;
            }
        }

        /// <summary>
        /// Returns the bullet object to the pool.
        /// </summary>
        /// <param name="target">Bullet object to release</param>
        /// <exception cref="UDENotBulletException">Thrown when the target object does not contain 
        /// <see cref="UDEBaseBullet"/> or its child script as the object's component</exception>
        public void ReleaseBullet(GameObject target)
        {
            if (target.GetComponent<UDEBaseBullet>() == null)
                throw new UDENotBulletException("The target object to get is not a bullet.")
                {
                    InvalidObjectName = target.name
                };

            target.SetActive(false);
        }
    }
}