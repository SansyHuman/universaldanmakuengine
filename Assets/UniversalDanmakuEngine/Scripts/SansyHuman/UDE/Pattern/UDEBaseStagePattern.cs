﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Object;

namespace SansyHuman.UDE.Pattern
{
    /// <summary>
    /// Base class of stage patterns. In this class, enemies are summoned and their patterns start.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class UDEBaseStagePattern : MonoBehaviour
    {
        /// <summary>
        /// All enemies summoned in this stage.
        /// </summary>
        [SerializeField]
        protected List<UDEEnemy> enemies;
        /// <summary>
        /// All sub boss characters summoned in this stage.
        /// </summary>
        [SerializeField]
        protected List<UDEEnemy> subBoss;
        /// <summary>
        /// All boss characters summoned in this stage.
        /// </summary>
        [SerializeField]
        protected List<UDEEnemy> boss;

        private IEnumerator stage = null;

        /// <summary>
        /// Starts the stage pattern.
        /// </summary>
        public void StartStage()
        {
            if (stage == null)
                stage = StagePattern();
            StartCoroutine(stage);
        }

        /// <summary>
        /// Stops the stage pattern.
        /// </summary>
        public void StopStage()
        {
            StopCoroutine(stage);
        }

        /// <summary>
        /// Method that represent the stage pattern. You can create various custom stage patterns
        /// implementing this method.
        /// </summary>
        /// <returns>Enumerator that represents the stage pattern</returns>
        protected abstract IEnumerator StagePattern();

        /// <summary>
        /// Summons the enemy.
        /// </summary>
        /// <param name="enemy">Enemy prefab to summon</param>
        /// <returns>Summoned enemy</returns>
        protected virtual UDEEnemy SummonEnemy(UDEEnemy enemy)
        {
            UDEEnemy summonedEnemy = Instantiate<GameObject>(enemy.gameObject).GetComponent<UDEEnemy>();
            summonedEnemy.gameObject.SetActive(true);
            return summonedEnemy;
        }
    }
}