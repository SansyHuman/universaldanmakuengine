﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Exception;

namespace SansyHuman.UDE.Pattern
{
    /// <summary>
    /// Base class for shot patterns of enemy.
    /// </summary>
    /// <remarks>
    /// You should implement abstract method <see cref="UDEBaseShotPattern.ShotPattern"/>
    /// to represent the pattern.
    /// </remarks>
    /// <seealso cref="UDEBaseShotPattern.ShotPattern"/>
    [DisallowMultipleComponent]
    public abstract class UDEBaseShotPattern : MonoBehaviour
    {
        /// <summary>
        /// Represent the type of the pattern. Use Special only when the pattern is a boss' special pattern.
        /// </summary>
        public enum PatternType
        {
            Normal, Special
        }

        /// <summary>
        /// Bullets that are used in the pattern. Bullet objects should have <see cref="UDEBaseBullet"/>
        /// or its children as a component.
        /// </summary>
        [SerializeField] protected List<GameObject> baseBullets;
        /// <summary>Type of the pattern.</summary>
        [SerializeField] protected PatternType type;
        /// <summary>Represents if the pattern ends after some time.</summary>
        [SerializeField] protected bool hasTimeLimit;
        /// <summary>Time limit of the pattern.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBaseShotPattern.hasTimeLimit"/> is <c>false</c>.</remarks>
        [SerializeField] protected float timeLimit;

        /// <summary>Collection of bullets that is shotted in this pattern.</summary>
        protected List<UDEBaseBullet> shottedBullets;
        /// <summary>Enemy object that shots the pattern.</summary>
        /// <remarks>Enemy object should have <see cref="UDEEnemy"/> as its component.</remarks>
        protected UDEEnemy originEnemy;
        /// <summary>Passed time from the start of the pattern.</summary>
        protected float time;
        /// <summary>Return value of <see cref="UDEBaseShotPattern.ShotPattern"/>.</summary>
        protected IEnumerator pattern;
        /// <summary>Represents whether the pattern is active.</summary>
        [SerializeField] protected bool shotPatternOn = false;

        public float Time { get => time; set => time = value; }
        public PatternType Type { get => type; set => type = value; }
        public bool HasTimeLimit { get => hasTimeLimit; }
        public float TimeLimit { get => timeLimit; }

        /// <summary>
        /// Initialize the pattern. This methods is called in <see cref="UDEEnemy.ManagePatterns"/>.
        /// </summary>
        /// <param name="originEnemy">The enemy object which shots the pattern.</param>
        public virtual void Initialize(UDEEnemy originEnemy)
        {
            shottedBullets = new List<UDEBaseBullet>(1024);
            this.originEnemy = originEnemy;
            Time = 0;
            pattern = ShotPattern();
        }

        private void FixedUpdate()
        {
            if (shotPatternOn)
                time += UnityEngine.Time.deltaTime * UDETime.Instance.EnemyTimeScale;
        }

        /// <summary>
        /// Start the pattern. This methods is called in <see cref="UDEEnemy.ManagePatterns"/>.
        /// </summary>
        public void StartPattern()
        {
            shotPatternOn = true;
            StartCoroutine(pattern);
        }

        /// <summary>
        /// Stop the pattern. This methods is called in <see cref="UDEEnemy.ManagePatterns"/>.
        /// </summary>
        public void StopPattern()
        {
            StopCoroutine(pattern);
            shotPatternOn = false;
        }

        /// <summary>
        /// Reset the pattern.
        /// </summary>
        /// <exception cref="UDEPatternRunningException">Thrown when the pattern is running on</exception>
        public void ResetPattern()
        {
            if (shotPatternOn)
                throw new UDEPatternRunningException("The pattern is running on. Cannot reset the pattern.");
            pattern = ShotPattern();
        }

        /// <summary>
        /// Add shotted bullet by this pattern. Only used internally in <see cref="UDEBaseBullet"/>.
        /// </summary>
        public void AddBullet(UDEBaseBullet bullet)
        {
            shottedBullets.Add(bullet);
        }

        public static int operator +(UDEBaseShotPattern pattern, UDEBaseBullet bullet)
        {
            pattern.AddBullet(bullet);
            return 0;
        }

        /// <summary>
        /// Remove shotted bullet by this pattern. Only used internally in <see cref="UDEBaseBullet"/>.
        /// </summary>
        public void RemoveBullet(UDEBaseBullet bullet)
        {
            shottedBullets.Remove(bullet);
        }

        public static int operator -(UDEBaseShotPattern pattern, UDEBaseBullet bullet)
        {
            pattern.RemoveBullet(bullet);
            return 0;
        }

        /// <summary>
        /// Method that represent the pattern. You can create various custom patterns
        /// implementing this method.
        /// </summary>
        /// <example>
        /// This example shots the pattern similar to 境符「波と粒の境界」 of Yukari Yakumo
        /// from Touhou Project TH09.5 Shoot the Bullet.
        /// <code>
        /// public class TestBossPattern : UDEBaseShotPattern
        /// {
        ///     [SerializeField]
        ///     private int NumberOfBullets = 10;
        ///     [SerializeField]
        ///     private float BulletSpeed = 2f;
        ///
        ///     protected override IEnumerator ShotPattern()
        ///     {
        ///         WaitForSeconds interval = new WaitForSeconds(0.07f);
        ///         float AngleDeg = (360f / NumberOfBullets);
        ///         float AngleRef = 0f;
        ///         float RefOmega = 0f;
        ///         while (true)
        ///         {
        ///             for (int i = 0; i < NumberOfBullets; i++)
        ///             {
        ///                 UDEBaseBullet bullet = UDEBulletPool.instance.GetBullet(baseBullets[0]);
        ///                 UDEBulletMovement movement = UDEPolarMovementBuilder.Create().RadialSpeed(BulletSpeed).InitialAngle(i * AngleDeg + AngleRef).Build();
        ///                 Vector2 origin = originEnemy.transform.position;
        ///                 var formLocTuple = UDEMath.Polar2Cartesian(0.7f, movement.angle);
        ///                 Vector2 formLocation = new Vector2(formLocTuple.x, formLocTuple.y) + origin;
        ///                 bullet.Initialize(formLocation, origin, 0, originEnemy, this, true, movement);
        ///             }
        ///             AngleRef += RefOmega;
        ///             RefOmega += 0.2f;
        ///             if (RefOmega >= 360)
        ///                 RefOmega -= 360;
        ///             if (AngleRef >= 360)
        ///                 AngleRef -= 360;
        ///             yield return interval;
        ///         }
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <returns>Enumerator that represents the pattern</returns>
        protected abstract IEnumerator ShotPattern();
    }
}