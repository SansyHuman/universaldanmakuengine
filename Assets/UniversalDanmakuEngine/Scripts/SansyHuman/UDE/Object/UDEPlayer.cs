﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections;
using UnityEngine;
using SansyHuman.UDE.Management;

namespace SansyHuman.UDE.Object
{
    public abstract class UDEPlayer : UDEBaseCharacter, IUDEControllable
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float slowModeSpeedAmplifier;
        [SerializeField]
        private float bulletFireInterval;

        private bool invincible = false;
        private IEnumerator shotCoroutine;

        public float Speed { get => speed; set => speed = value; }
        public float SlowModeSpeedAmplifier { get => slowModeSpeedAmplifier; set => slowModeSpeedAmplifier = value; }
        public float BulletFireInterval
        {
            get => bulletFireInterval;
            set
            {
                bulletFireInterval = value;
                if (bulletFireInterval < 0)
                    bulletFireInterval = 0;
            }
        }

        public void Move(float deltaTime)
        {
            float realSpeed = Speed;
            if (Input.GetKey(KeyCode.LeftShift))
                realSpeed *= slowModeSpeedAmplifier;

            Vector3 velocity = new Vector3();
            if (Input.GetKey(KeyCode.RightArrow))
                velocity += Vector3.right * realSpeed;
            if (Input.GetKey(KeyCode.LeftArrow))
                velocity += Vector3.left * realSpeed;
            if (Input.GetKey(KeyCode.UpArrow))
                velocity += Vector3.up * realSpeed;
            if (Input.GetKey(KeyCode.DownArrow))
                velocity += Vector3.down * realSpeed;

            characterTr.Translate(velocity * deltaTime * UDETime.Instance.PlayerTimeScale);

            Vector3 pos = Camera.main.WorldToViewportPoint(characterTr.position);
            if (pos.x < 0)
                pos.x = 0;
            if (pos.x > 1)
                pos.x = 1;
            if (pos.y < 0)
                pos.y = 0;
            if (pos.y > 1)
                pos.y = 1;
            characterTr.position = Camera.main.ViewportToWorldPoint(pos);
        }

        public abstract IEnumerator ShotBullet();

        // Start is called before the first frame update
        protected override void Awake()
        {
            base.Awake();
            shotCoroutine = ShotBullet();
            StartCoroutine(shotCoroutine);
        }

        // Update is called once per frame
        void Update()
        {
            Move(Time.deltaTime);
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (invincible)
                return;

            if (collision.CompareTag("Enemy"))
                StartCoroutine(DamageSelf());
            else if (collision.CompareTag("Bullet"))
            {
                UDEBaseBullet bullet = collision.GetComponent<UDEBaseBullet>();
                if (bullet != null && bullet.OriginCharacter is UDEEnemy)
                {
                    UDEBulletPool.Instance.ReleaseBullet(bullet.gameObject);
                    StartCoroutine(DamageSelf());
                }
            }
        }

        private IEnumerator DamageSelf()
        {
            health--;
            invincible = true;
            SpriteRenderer renderer = self.GetComponent<SpriteRenderer>();
            Color col = renderer.color;
            col.a = 0.5f;
            renderer.color = col;
            yield return StartCoroutine(UDETime.Instance.WaitForScaledSeconds(3f, UDETime.TimeScale.PLAYER));
            col.a = 1f;
            renderer.color = col;
            invincible = false;
        }
    }
}