﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Util.Math;
using static SansyHuman.UDE.Object.UDEBulletMovement;

namespace SansyHuman.UDE.Object
{
    /// <summary>
    /// The Base class of all bullets.
    /// </summary>
    [AddComponentMenu("UDE/Bullet/Base Bullet")]
    [DisallowMultipleComponent]
    public class UDEBaseBullet : MonoBehaviour
    {
        #region Fields
        [SerializeField] private UDEBaseCharacter originCharacter;
        [SerializeField] private UDEBaseShotPattern originShotPattern;
        [SerializeField] private UDETime.TimeScale usingTimeScale;
        [SerializeField] private float damage;

        [SerializeField] private Vector2 origin; // Original position
        [SerializeField] private bool setOriginToCharacter; // Change origin according to origin character
        [SerializeField] protected Vector2 position; // Current position
        [SerializeField] private float rotation; // rotation angle. The angle's unit is degree and horizontal right is 0

        [SerializeField] private float r; // Distance from origin in polar coordinate system
        [SerializeField] private float angle; // Angle in polar coordinate system

        protected int phase = 0;
        protected float time = 0; //passed time from initialize
        [SerializeField] private float summonTime = 0.08f;
        [SerializeField] private bool isSummoning = false;

        private UDEBulletManager.BulletMoveHandler moveHandler;

        protected UDEBulletMovement[] movements;

        /// <value>Gets the current phase of the bullet.</value>
        public int Phase { get => phase; }
        /// <value>Gets and sets the damage of the bullet.</value>
        public float Damage
        {
            get => damage;
            set
            {
                damage = value;
                if (damage < 0)
                    damage = 0;
            }
        }
        /// <value>Gets the square of distance of the bullet from origin.</value>
        public float SqrMagnitudeFromOrigin { get => Vector2.SqrMagnitude(position - origin); }
        /// <value>Gets the current movement.</value>
        public ref UDEBulletMovement CurrentMovement { get => ref movements[phase]; }
        /// <value>Gets the movement in phase.</value>
        public ref UDEBulletMovement this[int phase] { get => ref movements[phase]; }
        /// <value>Sets the summon time(default value is 0.08 seconds.</value>
        public float SummonTime { set { summonTime = value; } }
        /// <value>Gets the character that shot the bullet.</value>
        public UDEBaseCharacter OriginCharacter { get => originCharacter; }
        #endregion

        private new SpriteRenderer renderer;
        private new Collider2D collider;
        private Behaviour halo;

        private void Awake()
        {
            renderer = gameObject.GetComponent<SpriteRenderer>();
            collider = gameObject.GetComponent<Collider2D>();
            halo = gameObject.GetComponent("Halo") as Behaviour;
        }

        #region Initialize and Dispose
        protected virtual void OnEnable()
        {
            time = 0;
            phase = 0;
            if (moveHandler == null)
                moveHandler = new UDEBulletManager.BulletMoveHandler(MoveBullet);
            if (originShotPattern != null)
                _ = originShotPattern + this;

            if (halo != null)
            {
                renderer.enabled = false;
                collider.enabled = false;
                isSummoning = true;
            }
        }

        protected virtual void OnDisable()
        {
            UDEBulletManager.Instance.MoveBullets -= moveHandler;
            if (originShotPattern != null)
                _ = originShotPattern - this;

            if (halo != null)
                halo.enabled = true;
        }

        protected GameObject self;
        protected Transform bulletTr;
        protected Transform originChrTr;

        /// <summary>
        /// Initializes the bullet.
        /// Sets the initial position and movements of the bullet.
        /// </summary>
        /// <param name="initPos">The initial position of the bullet</param>
        /// <param name="origin">Origin in polar coordinate</param>
        /// <param name="originCharacter"><see cref="UDEBaseCharacter"/> instance which shot the bullet</param>
        /// <param name="originShotPattern"><see cref="UDEBaseShotPattern"/> instance which summoned the bullet. It is nullable</param>
        /// <param name="setOriginToCharacter">Whether set the origin in polar coordinate to origin character's position</param>
        /// <param name="movements">Movements of the bullet. All <see cref="UDEBulletMovement"/> should be in the order of phase</param>
        public virtual void Initialize(Vector2 initPos, Vector2 origin, float initialRotation, UDEBaseCharacter originCharacter, UDEBaseShotPattern originShotPattern, bool setOriginToCharacter, params UDEBulletMovement[] movements)
        {
            this.position = initPos;
            this.origin = origin;
            this.rotation = initialRotation;
            this.originCharacter = originCharacter;
            this.originShotPattern = originShotPattern;
            this.setOriginToCharacter = setOriginToCharacter;
            this.movements = movements;

            r = (initPos - origin).magnitude;
            angle = r > 0.01f ? UDEMath.Deg(initPos - origin) : movements[0].angle;
            if (movements[0].faceToMovingDirection)
            {
                switch (movements[0].mode)
                {
                    case MoveMode.CartesianMode:
                        rotation = movements[0].angle;
                        break;
                    case MoveMode.PolarMode:
                        rotation = movements[0].angularSpeed > 0 ? movements[0].angle + 90 : movements[0].angle - 90;
                        if (movements[0].angularSpeed == 0)
                            rotation = movements[0].angle;
                        break;
                }
            }

            if (this.originCharacter is UDEEnemy)
                this.usingTimeScale = UDETime.TimeScale.ENEMY;
            else if (this.originCharacter is UDEPlayer)
                this.usingTimeScale = UDETime.TimeScale.PLAYER;
            else
                this.usingTimeScale = UDETime.TimeScale.UNSCALED;

            self = gameObject;
            bulletTr = transform;
            originChrTr = this.originCharacter.transform;

            UDEBulletManager.Instance.MoveBullets += moveHandler;
            bulletTr.SetPositionAndRotation(position, Quaternion.Euler(0, 0, rotation));
        }
        #endregion

        #region Bullet Movement
        /// <summary>
        /// Moves the bullet.
        /// </summary>
        /// <param name="deltaTime">
        /// <para>The passed time from the previous frame.</para>
        /// <para>It is recommended to pass <see cref="UnityEngine.Time.deltaTime"/></para>
        /// </param>
        public virtual void MoveBullet(float deltaTime)
        {
            float scaledDeltaTime;
            switch (usingTimeScale)
            {
                case UDETime.TimeScale.ENEMY:
                    scaledDeltaTime = deltaTime * UDETime.Instance.EnemyTimeScale;
                    break;
                case UDETime.TimeScale.PLAYER:
                    scaledDeltaTime = deltaTime * UDETime.Instance.PlayerTimeScale;
                    break;
                case UDETime.TimeScale.UNSCALED:
                default:
                    scaledDeltaTime = deltaTime;
                    break;
            }
            time += scaledDeltaTime;

            if (halo != null) // If there is halo on bullet, the bullet will start to move after the halo is active for summoning time.
            {
                if (isSummoning)
                {
                    if (time > summonTime)
                    {
                        renderer.enabled = true;
                        collider.enabled = true;
                        halo.enabled = false;
                        isSummoning = false;
                        time = 0;
                    }
                    return;
                }
            }

            while (!(phase >= movements.Length - 1) && time >= movements[phase + 1].startTime)
                phase += 1;
            if (movements[phase].hasEndTime && time >= movements[phase].endTime)
                return;
            if (phase == 0 && time < movements[phase].startTime)
                return;

            ref UDEBulletMovement currentMovement = ref movements[phase];
            Vector2 displacement;
            switch (currentMovement.mode)
            {
                case MoveMode.CartesianMode:
                    displacement = CartesianMove(scaledDeltaTime, ref currentMovement);
                    break;
                case MoveMode.PolarMode:
                    displacement = PolarMove(scaledDeltaTime, ref currentMovement);
                    break;
                default:
                    displacement = new Vector2(0, 0);
                    break;
            }

            position += displacement;
            if (currentMovement.mode == MoveMode.CartesianMode)
            {
                if (setOriginToCharacter)
                    origin = originChrTr.position;
                r = (position - origin).magnitude;
                angle = r > 0.01f ? UDEMath.Deg(position - origin) : rotation;
            }
            bulletTr.SetPositionAndRotation(position, Quaternion.Euler(0, 0, rotation));
            currentMovement.updateCount++;
        }

        /*
         * Calculates the displacement of the bullet in cartesian coordinate system.
         */
        private Vector2 CartesianMove(float deltaTime, ref UDEBulletMovement movement)
        {
            float dx = movement.speed * Mathf.Cos(movement.angle * Mathf.Deg2Rad) * deltaTime;
            float dy = movement.speed * Mathf.Sin(movement.angle * Mathf.Deg2Rad) * deltaTime;
            if (movement.faceToMovingDirection)
                rotation = movement.angle;
            else
                SetRotation(deltaTime, ref movement);
            movement.speed += movement.tangentialAccel * deltaTime;
            movement.angle += movement.normalAccel * deltaTime;
            while (movement.angle > 360)
                movement.angle -= 360;
            while (movement.angle < 0)
                movement.angle += 360;
            if (movement.limitSpeed)
            {
                if (movement.speed > movement.maxSpeed)
                    movement.speed = movement.maxSpeed;
                if (movement.speed < movement.minSpeed)
                    movement.speed = movement.minSpeed;
            }
            Vector2 displacement = new Vector2(dx, dy);
            return displacement;
        }

        /*
         * Calculates the displacement of the bullet in polar coordinate system.
         */
        private Vector2 PolarMove(float deltaTime, ref UDEBulletMovement movement)
        {
            float xPre, yPre;
            UDEMath.Polar2Cartesian(r, angle, out xPre, out yPre);
            Vector2 rPre = new Vector2(xPre, yPre);

            r += movement.radialSpeed * deltaTime;
            movement.radialSpeed += movement.radialAccel * deltaTime;
            angle += movement.angularSpeed * deltaTime;
            movement.angularSpeed += movement.angularAccel * deltaTime;
            if (movement.limitSpeed)
            {
                if (movement.radialSpeed > movement.maxRadialSpeed)
                    movement.radialSpeed = movement.maxRadialSpeed;
                if (movement.radialSpeed < movement.minRadialSpeed)
                    movement.radialSpeed = movement.minRadialSpeed;
                if (movement.angularSpeed > movement.maxAngularSpeed)
                    movement.angularSpeed = movement.maxAngularSpeed;
                if (movement.angularSpeed < movement.minAngularSpeed)
                    movement.angularSpeed = movement.minAngularSpeed;
            }
            float x, y;
            UDEMath.Polar2Cartesian(r, angle, out x, out y);
            movement.angle = angle;

            Vector2 originDisplacement = Vector2.zero;
            if (setOriginToCharacter)
            {
                originDisplacement = originCharacter.transform.position - new Vector3(origin.x, origin.y);
                origin = originChrTr.position;
            }

            Vector2 displacement = new Vector2(x, y) - rPre;
            if (movement.faceToMovingDirection)
                rotation = displacement.sqrMagnitude > 0.0001f ? UDEMath.Deg(displacement) : rotation;
            else
                SetRotation(deltaTime, ref movement);

            return displacement + originDisplacement;
        }

        /*
         * Sets rotation if faceToMovingDirection in UDEBulletMovement is false.
         */
        private void SetRotation(float deltaTime, ref UDEBulletMovement movement)
        {
            rotation += movement.rotationAngularSpeed * deltaTime;
            while (rotation > 360)
                rotation -= 360;
            while (rotation < 0)
                rotation += 360;
            movement.rotationAngularSpeed += movement.rotationAngularAcceleration * deltaTime;
            if (movement.limitRotationSpeed)
            {
                if (movement.rotationAngularSpeed > movement.maxRotationSpeed)
                    movement.rotationAngularSpeed = movement.maxRotationSpeed;
                if (movement.rotationAngularSpeed < movement.minRotationSpeed)
                    movement.rotationAngularSpeed = movement.minRotationSpeed;
            }
        }
        #endregion

        /// <summary>
        /// Sets forcefully to the next phase of the bullet.
        /// </summary>
        public void ForceMoveToNextPhase()
        {
            time = movements[phase + 1].startTime;
        }

        /// <summary>
        /// Sets forcefully to the phase.
        /// </summary>
        /// <param name="phase">Phase to set</param>
        public void ForceMoveToPhase(int phase)
        {
            time = movements[phase].startTime;
        }

        /// <summary>
        /// Override of the operator <c>++</c>. Sets forcefully to the next phase.
        /// </summary>
        /// <param name="bullet">Bullet to set the phase to next</param>
        /// <returns>Itself</returns>
        public static UDEBaseBullet operator ++(UDEBaseBullet bullet)
        {
            bullet.ForceMoveToNextPhase();
            return bullet;
        }
    }
}