﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Util.Math;

namespace SansyHuman.UDE.Object
{
    public class UDECurveLaser : UDELaser
    {
        [SerializeField] private float headDuration;
        [SerializeField] private float pointDuration;

        private List<float> pointAddTimes;

        public enum HeadMoveMode
        {
            CartesianFunction, PolarFunction
        }

        private HeadMoveMode mode;

        private UDEMath.CartesianTimeFunction cartesianHeadFunction;
        private UDEMath.PolarTimeFunction polarHeadFunction;

        private void InitializeInternal(float headDuration, float pointDuration)
        {
            this.headDuration = headDuration;
            this.pointDuration = pointDuration;
            pointAddTimes = new List<float>(1);
            pointAddTimes.Add(0);
            laserHeadTr = laserHead.transform;
        }

        public void Initialize(Vector2 origin,
                               UDEBaseCharacter originCharacter,
                               bool followOriginCharacter,
                               Vector2 initialLocalHeadLocation,
                               UDETime.TimeScale timeScale,
                               float headDuration,
                               float pointDuration,
                               UDEMath.CartesianTimeFunction headFunction)
        {
            Initialize(origin, originCharacter, followOriginCharacter, initialLocalHeadLocation, timeScale);
            InitializeInternal(headDuration, pointDuration);
            cartesianHeadFunction = headFunction;
            mode = HeadMoveMode.CartesianFunction;
        }

        public void Initialize(Vector2 origin,
                               UDEBaseCharacter originCharacter,
                               bool followOriginCharacter,
                               Vector2 initialLocalHeadLocation,
                               UDETime.TimeScale timeScale,
                               float headDuration,
                               float pointDuration,
                               UDEMath.PolarTimeFunction headFunction)
        {
            Initialize(origin, originCharacter, followOriginCharacter, initialLocalHeadLocation, timeScale);
            InitializeInternal(headDuration, pointDuration);
            polarHeadFunction = headFunction;
            mode = HeadMoveMode.PolarFunction;
        }

        private Transform laserHeadTr;

        protected override void ExtendLaser(float deltaTime)
        {
            base.ExtendLaser(deltaTime);

            float scaledDeltaTime = deltaTime * UDETime.Instance.GetTimeScale(timeScale);
            time += scaledDeltaTime;

            if (laserHead == null)
                goto PointsCheck;

            switch (mode)
            {
                case HeadMoveMode.CartesianFunction:
                    laserHeadTr.localPosition = cartesianHeadFunction(time).ToVector2();
                    break;
                case HeadMoveMode.PolarFunction:
                    laserHeadTr.localPosition = ((UDEMath.CartesianCoord)polarHeadFunction(time)).ToVector2();
                    break;
            }

            if ((Vector2)laserHeadTr.localPosition == points[points.Count - 1])
                goto PointsCheck;

            points.Add(laserHeadTr.localPosition);
            pointAddTimes.Add(time);

            PointsCheck:
            for (int i = 0; i < points.Count; i++)
            {
                if (time - pointAddTimes[i] > pointDuration)
                {
                    points.RemoveAt(i);
                    pointAddTimes.RemoveAt(i);
                    i--;
                }
                else
                    break;
            }

            laser.positionCount = points.Count;
            for (int i = 0; i < points.Count; i++)
                laser.SetPosition(i, points[i]);
            laserCollider.points = points.ToArray();

            if (time > headDuration)
            {
                Destroy(laserHead);
                laserHead = null;
            }

            if (points.Count == 0)
                DestroyLaser();
        }
    }
}