﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace SansyHuman.UDE.Object
{
    /// <summary>
    /// Struct to save the movement of the bullet. All time units are seconds and all angle units are degrees.
    /// </summary>
    public struct UDEBulletMovement
    {
        /// <summary>
        /// <para>Represents the bullet's move. There are two types of move mode;
        /// Cartesian, which the velocity is expressed with speed and angle,</para>
        /// <para>and Polar, which the coordinate is expressed with distance from the origin and angle.</para>
        /// </summary>
        public enum MoveMode
        {
            CartesianMode, PolarMode
        }

        #region Basic variables
        /// <summary>Bullet's move mode. See <see cref="UDEBaseBullet.MoveMode"/>.</summary>
        public MoveMode mode;
        /// <summary>Time to start the movement.</summary>
        public float startTime;
        /// <summary>Time to end the movement.</summary>
        /// <remarks>It is recommended to set to 0 if <see cref="UDEBulletMovement.hasEndTime"/> is <c>false</c>.</remarks>
        public float endTime;
        /// <summary>Shows if the movement has the end time.</summary>
        public bool hasEndTime;
        /// <summary>Shows if there is a limit to speed of the bullet.</summary>
        public bool limitSpeed;
        #endregion

        /// <summary>Shows how many time the movement is used in the method <see cref="UDEBaseBullet.MoveBullet(float)"/>.</summary>
        public int updateCount;

        #region Cartesian mode variables
        //CartesianMode
        /// <summary>The bullet's speed in cartesian coordinate system.</summary>
        public float speed;
        /// <summary>The bullet's maximum speed in cartesian coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float maxSpeed;
        /// <summary>The bullet's minimum speed in cartesian coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float minSpeed;
        /// <summary>The bullet's movement angle in cartesian coordinate system.</summary>
        /// <remarks>The horizontal right direction is 0 degree.
        /// In polar mode, this variable is set to angle in polar coordinate system if the distance from origin is 0.</remarks>
        public float angle;
        /// <summary>The bullet's acceleration in the direction of the movement.</summary>
        public float tangentialAccel;
        /// <summary>The bullet's acceleration perpendicular to the movement.</summary>
        /// <remarks>It is actually the change of the <see cref="UDEBulletMovement.angle"/></remarks>
        public float normalAccel;
        #endregion

        #region Polar mode variables
        //PolarMode
        /// <summary>The bullet's radial speed in polar coordinate system.</summary>
        public float radialSpeed;
        /// <summary>The bullet's angular speed in polar coordinate system.</summary>
        public float angularSpeed;
        /// <summary>The bullet's maximum radial speed in polar coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float maxRadialSpeed;
        /// <summary>The bullet's maximum angular speed in polar coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float maxAngularSpeed;
        /// <summary>The bullet's minimum radial speed in polar coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float minRadialSpeed;
        /// <summary>The bullet's minimum angular speed in polar coordinate system.</summary>
        /// <remarks>It is recommended to set 0 if <see cref="UDEBulletMovement.limitSpeed"/> is <c>false</c>.</remarks>
        public float minAngularSpeed;
        /// <summary>The bullet's radial acceleration in polar coordinate system.</summary>
        public float radialAccel;
        /// <summary>The bullet's angular acceleration in polar coordinate system.</summary>
        public float angularAccel;
        #endregion

        #region Rotation variables
        /// <summary>Shows if the bullet automatically rotate to the direction of the movement.</summary>
        public bool faceToMovingDirection;
        /// <summary>Angular spped of bullet's rotation.</summary>
        public float rotationAngularSpeed;
        /// <summary>Angular acceleration of bullet's rotation.</summary>
        public float rotationAngularAcceleration;
        /// <summary>Shows if bullet's rotation speed has a limit.</summary>
        public bool limitRotationSpeed;
        /// <summary>Minimum rotation speed of the bullet.</summary>
        public float minRotationSpeed;
        /// <summary>Maximum rotation speed of the bullet.</summary>
        public float maxRotationSpeed;
        #endregion

        /// <summary>
        /// The basic constructor of the class <see cref="UDEBulletMovement"/>.
        /// </summary>
        /// <param name="mode">Move mode</param>
        /// <param name="startTime">Start time to move</param>
        /// <param name="endTime">End time to move</param>
        /// <param name="hasEndTime">True if it has an end time</param>
        /// <param name="limitSpeed">True if there is a speed limit</param>
        /// <param name="speed">Speed in cartesian coordinate system</param>
        /// <param name="maxSpeed">Maximum speed in cartesian coordinate system</param>
        /// <param name="minSpeed">Minimum speed in cartesian coordinate system</param>
        /// <param name="angle">Angle to move</param>
        /// <param name="tangentialAccel">Tangential acceleration in cartesian coordinate system</param>
        /// <param name="normalAccel">Normal acceleration in cartesian coordinate system</param>
        /// <param name="radialSpeed">Radial speed in polar coordinate system</param>
        /// <param name="angularSpeed">Angular speed in polar coordinate system</param>
        /// <param name="maxRadialSpeed">Maximum radial speed in polar coordinate system</param>
        /// <param name="maxAngularSpeed">Maximum angular speed in polar coordinate system</param>
        /// <param name="minRadialSpeed">Minimum radial speed in polar coordinate system</param>
        /// <param name="minAngularSpeed">Minimum angular speed in polar coordinate system</param>
        /// <param name="radialAccel">Radial acceleration in polar coordinate system</param>
        /// <param name="angularAccel">Angular acceleration in polar coordinate system</param>
        public UDEBulletMovement(MoveMode mode,
                                 float startTime,
                                 float endTime,
                                 bool hasEndTime,
                                 bool limitSpeed,
                                 float speed,
                                 float maxSpeed,
                                 float minSpeed,
                                 float angle,
                                 float tangentialAccel,
                                 float normalAccel,
                                 float radialSpeed,
                                 float angularSpeed,
                                 float maxRadialSpeed,
                                 float maxAngularSpeed,
                                 float minRadialSpeed,
                                 float minAngularSpeed,
                                 float radialAccel,
                                 float angularAccel,
                                 bool faceToMovingDirection,
                                 float rotationAngularSpeed,
                                 float rotationAngularAcceleration,
                                 bool limitRotationSpeed,
                                 float minRotationSpeed,
                                 float maxRotationSpeed)
        {
            this.mode = mode;
            this.startTime = startTime;
            this.endTime = endTime;
            this.hasEndTime = hasEndTime;
            this.limitSpeed = limitSpeed;
            this.updateCount = 0;
            this.speed = speed;
            this.maxSpeed = maxSpeed;
            this.minSpeed = minSpeed;
            this.angle = angle;
            this.tangentialAccel = tangentialAccel;
            this.normalAccel = normalAccel;
            this.radialSpeed = radialSpeed;
            this.angularSpeed = angularSpeed;
            this.maxRadialSpeed = maxRadialSpeed;
            this.maxAngularSpeed = maxAngularSpeed;
            this.minRadialSpeed = minRadialSpeed;
            this.minAngularSpeed = minAngularSpeed;
            this.radialAccel = radialAccel;
            this.angularAccel = angularAccel;
            this.faceToMovingDirection = faceToMovingDirection;
            this.rotationAngularSpeed = rotationAngularSpeed;
            this.rotationAngularAcceleration = rotationAngularAcceleration;
            this.limitRotationSpeed = limitRotationSpeed;
            this.minRotationSpeed = minRotationSpeed;
            this.maxRotationSpeed = maxRotationSpeed;
        }

        #region Basic builders
        /// <summary>
        /// Creat the <see cref="UDEBulletMovement"/> instance that represents the cartesian mode movement without speed limit.
        /// </summary>
        /// <param name="startTime">Start time to move</param>
        /// <param name="speed">Speed in cartesian coordinate system</param>
        /// <param name="angle">Angle to move</param>
        /// <param name="tangentialAccel">Tangential acceleration in cartesian coordinate system</param>
        /// <param name="normalAccel">Normal acceleration in cartesian coordinate system</param>
        /// <returns><see cref="UDEBulletMovement"/> instance in cartesian mode without speed limit</returns>
        public static UDEBulletMovement GetCartesianMovement(float startTime, float speed, float angle, float tangentialAccel, float normalAccel)
        {
            return new UDEBulletMovement(MoveMode.CartesianMode, startTime, 0, false, false,
                speed, 0, 0, angle, tangentialAccel, normalAccel, 0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, false, 0, 0);
        }

        /// <summary>
        /// Creat the <see cref="UDEBulletMovement"/> instance that represents the cartesian mode movement with speed limit.
        /// </summary>
        /// <param name="startTime">Start time to move</param>
        /// <param name="speed">Speed in cartesian coordinate system</param>
        /// <param name="maxSpeed">Maximum speed in cartesian coordinate system</param>
        /// <param name="minSpeed">Minimum speed in cartesian coordinate system</param>
        /// <param name="angle">Angle to move</param>
        /// <param name="tangentialAccel">Tangential acceleration in cartesian coordinate system</param>
        /// <param name="normalAccel">Normal acceleration in cartesian coordinate system</param>
        /// <returns><see cref="UDEBulletMovement"/> instance in cartesian mode with speed limit</returns>
        public static UDEBulletMovement GetCartesianMovementWithSpeedLimit(float startTime, float speed, float maxSpeed, float minSpeed,
            float angle, float tangentialAccel, float normalAccel)
        {
            return new UDEBulletMovement(MoveMode.CartesianMode, startTime, 0, false, true, speed, maxSpeed, minSpeed, angle, tangentialAccel, normalAccel,
                0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, false, 0, 0);
        }

        /// <summary>
        /// Creat the <see cref="UDEBulletMovement"/> instance that represents the polar mode movement without speed limit.
        /// </summary>
        /// <param name="startTime">Start time to move</param>
        /// <param name="radialSpeed">Radial speed in polar coordinate system</param>
        /// <param name="angularSpeed">Angular speed in polar coordinate system</param>
        /// <param name="radialAccel">Radial acceleration in polar coordinate system</param>
        /// <param name="angularAccel">Angular acceleration in polar coordinate system</param>
        /// <returns><see cref="UDEBulletMovement"/> instance in polar mode without speed limit</returns>
        public static UDEBulletMovement GetPolarMovement(float startTime, float radialSpeed, float angularSpeed, float radialAccel, float angularAccel)
        {
            return new UDEBulletMovement(MoveMode.PolarMode, startTime, 0, false, false, 0, 0, 0, 0, 0, 0,
                radialSpeed, angularSpeed, 0, 0, 0, 0, radialAccel, angularAccel, true, 0, 0, false, 0, 0);
        }

        /// <summary>
        /// Creat the <see cref="UDEBulletMovement"/> instance that represents the polar mode movement with speed limit.
        /// </summary>
        /// <param name="startTime">Start time to move</param>
        /// <param name="radialSpeed">Radial speed in polar coordinate system</param>
        /// <param name="angularSpeed">Angular speed in polar coordinate system</param>
        /// <param name="maxRadialSpeed">Maximum radial speed in polar coordinate system</param>
        /// <param name="maxAngularSpeed">Maximum angular speed in polar coordinate system</param>
        /// <param name="minRadialSpeed">Minimum radial speed in polar coordinate system</param>
        /// <param name="minAngularSpeed">Minimum angular speed in polar coordinate system</param>
        /// <param name="radialAccel">Radial acceleration in polar coordinate system</param>
        /// <param name="angularAccel">Angular acceleration in polar coordinate system</param>
        /// <returns><see cref="UDEBulletMovement"/> instance in polar mode with speed limit</returns>
        public static UDEBulletMovement GetPolarMovementWithSpeedLimit(float startTime, float radialSpeed, float angularSpeed,
            float maxRadialSpeed, float maxAngularSpeed, float minRadialSpeed, float minAngularSpeed, float radialAccel, float angularAccel)
        {
            return new UDEBulletMovement(MoveMode.PolarMode, startTime, 0, false, false, 0, 0, 0, 0, 0, 0,
                radialSpeed, angularSpeed, maxRadialSpeed, maxAngularSpeed, minRadialSpeed, minAngularSpeed, radialAccel, angularAccel, true, 0, 0, false, 0, 0);
        }

        /// <summary>
        /// Creat the <see cref="UDEBulletMovement"/> instance that has no movement.
        /// </summary>
        public static UDEBulletMovement GetNoMovement()
        {
            return new UDEBulletMovement(MoveMode.CartesianMode, 0, 0, false, false, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, true, 0, 0, false, 0, 0);
        }
        #endregion

        /// <summary>
        /// Override of <see cref="object.ToString"/>.
        /// </summary>
        /// <returns>String contains informations of the movement</returns>
        public override string ToString()
        {
            string str = "Move Mode: " + mode.ToString();
            str += "\nStart Time: " + startTime.ToString();
            if (hasEndTime)
                str += "\nEnd Time: " + endTime.ToString();
            switch (this.mode)
            {
                case MoveMode.CartesianMode:
                    str += "\n\nSpeed: " + speed.ToString();
                    if (limitSpeed)
                    {
                        str += "\nMinimum Speed: " + minSpeed.ToString();
                        str += "\nMaximum Speed: " + maxSpeed.ToString();
                    }
                    str += "\nMoving Angle: " + angle.ToString();
                    str += "\nTangential Acceleration: " + tangentialAccel.ToString();
                    str += "\nNormal Acceleration: " + normalAccel.ToString();
                    break;
                case MoveMode.PolarMode:
                    str += "\n\nRadial Speed: " + radialSpeed.ToString();
                    str += "\nAngular Speed: " + angularSpeed.ToString();
                    if (limitSpeed)
                    {
                        str += "\nMinimum Radial Speed: " + minRadialSpeed.ToString();
                        str += "\nMaximum Radial Speed: " + maxRadialSpeed.ToString();
                        str += "\nMinimum Angular Speed: " + minAngularSpeed.ToString();
                        str += "\nMaximum Angular Speed: " + maxAngularSpeed.ToString();
                    }
                    str += "\nRadial Acceleration: " + radialAccel.ToString();
                    str += "\nAngular Acceleration: " + angularAccel.ToString();
                    break;
            }
            if (faceToMovingDirection)
                str += "\n\nThe bullet is facing to moving direction.";
            else
            {
                str += "\n\nRotational Angular Speed: " + rotationAngularSpeed.ToString();
                str += "\nRotational Angular Acceleration: " + rotationAngularAcceleration.ToString();
                if (limitRotationSpeed)
                {
                    str += "\nMinimum Rotational Speed: " + minRotationSpeed.ToString();
                    str += "\nMaximum Rotational Speed: " + maxRotationSpeed.ToString();
                }
            }
            return str;
        }
    }
}