﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Management;

namespace SansyHuman.UDE.Object
{
    /// <summary>
    /// Class of all enemies.
    /// </summary>
    [AddComponentMenu("UDE/Character/Enemy")]
    [DisallowMultipleComponent]
    public class UDEEnemy : UDEBaseCharacter
    {
        /// <summary>
        /// Class contains a shot pattern and it's health.
        /// </summary>
        [Serializable]
        public class ShotPattern
        {
            public int health;
            public UDEBaseShotPattern shotPattern;
        }

        /// <summary>
        /// List of shot patterns enemy will shot.
        /// </summary>
        [SerializeField] protected List<ShotPattern> shotPatterns;
        /// <summary>
        /// Score when the enemy dies.
        /// </summary>
        [SerializeField] protected int scoreOnDeath;
        /// <summary>
        /// Shows whether the enemy is killable.
        /// </summary>
        [SerializeField] protected bool canBeDamaged;

        protected int currentPhase;

        public int ScoreOnDeath
        {
            get => scoreOnDeath;
            set
            {
                scoreOnDeath = value;
                if (scoreOnDeath < 0)
                    scoreOnDeath = 0;
            }
        }

        /// <summary>
        /// Initializes the enemy.
        /// </summary>
        public virtual void Initialize()
        {
            alive = true;
            canBeDamaged = true;
            StartCoroutine(ManagePatterns());
        }

        /// <summary>
        /// Gets current shot pattern
        /// </summary>
        /// <returns><see cref="UDEBaseShotPattern"/> instance currently running on.</returns>
        public UDEBaseShotPattern GetCurrentPattern()
        {
            return shotPatterns[currentPhase].shotPattern;
        }

        private IEnumerator ManagePatterns()
        {
            for (int i = 0; i < shotPatterns.Count; i++)
            {
                currentPhase = i;
                health = shotPatterns[i].health;
                shotPatterns[i].shotPattern.gameObject.SetActive(true);
                UDEBaseShotPattern pattern = shotPatterns[i].shotPattern;
                pattern.Initialize(this);
                pattern.StartPattern();
                yield return new WaitUntil(() =>
                    (health < 0 || (pattern.HasTimeLimit && pattern.Time > pattern.TimeLimit)));
                pattern.StopPattern();
            }
            alive = false;
            OnDeath();
        }

        /// <summary>
        /// Override method of <see cref="UDEBaseCharacter.OnDeath"/>
        /// </summary>
        public override void OnDeath()
        {
            base.OnDeath();
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (!canBeDamaged)
                return;

            if (collision.CompareTag("Bullet"))
            {
                UDEBaseBullet bullet = collision.GetComponent<UDEBaseBullet>();
                if (bullet != null && bullet.OriginCharacter is UDEPlayer)
                {
                    health -= bullet.Damage;
                    UDEBulletPool.Instance.ReleaseBullet(bullet.gameObject);
                }
            }
        }
    }
}