﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SansyHuman.UDE.Management;

namespace SansyHuman.UDE.Object
{
    public class UDEStraightLaser : UDELaser
    {
        private Vector2 headVelocity;

        [SerializeField] private float maxLaserLength = 0;

        private Vector2 maxHeadLocation;

        public void Initialize(Vector2 origin,
                               UDEBaseCharacter originCharacter,
                               bool followOriginCharacter,
                               Vector2 initialLocalHeadLocation,
                               UDETime.TimeScale timeScale,
                               Vector2 headVelocity,
                               float maxLaserLength)
        {
            Initialize(origin, originCharacter, followOriginCharacter, initialLocalHeadLocation, timeScale);
            points.Add(initialLocalHeadLocation);
            laser.positionCount = 2;
            laser.SetPosition(1, points[1]);
            laserCollider.points = points.ToArray();

            this.headVelocity = headVelocity;
            this.maxLaserLength = maxLaserLength;
            maxHeadLocation = initialLocalHeadLocation + headVelocity * maxLaserLength / headVelocity.magnitude;
        }

        protected override void ExtendLaser(float deltaTime)
        {
            base.ExtendLaser(deltaTime);
            float scaledDeltaTime = deltaTime * UDETime.Instance.GetTimeScale(timeScale);
            time += scaledDeltaTime;
            if (points[1] == maxHeadLocation)
                return;

            points[1] += headVelocity * scaledDeltaTime;
            if ((points[1] - points[0]).magnitude > maxLaserLength)
                points[1] = maxHeadLocation;

            laser.SetPosition(1, points[1]);
            laserCollider.points = points.ToArray();
        }
    }
}