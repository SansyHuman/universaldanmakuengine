﻿// Copyright (c) 2019 Subo Lee (KAIST HAJE)
// Please direct any bugs/comments/suggestions to suboo0308@gmail.com
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Management;

namespace SansyHuman.UDE.Object
{
    /// <summary>
    /// Base class of all lasers.
    /// </summary>
    public abstract class UDELaser : MonoBehaviour
    {
        protected LineRenderer laser;
        protected EdgeCollider2D laserCollider;

        protected Vector2 origin; // Origin of local position of the laser in world space
        protected UDEBaseCharacter originCharacter; // Character who fired the laser
        protected bool followOriginCharacter;

        protected GameObject laserHead;
        protected List<Vector2> points;
        protected UDETime.TimeScale timeScale;

        protected float time;

        [SerializeField] private float colliderWidthMultiplier = 0.7f;

        private UDEBulletManager.BulletMoveHandler extendHandler;
        private Transform laserTr;

        private void Awake()
        {
            laser = GetComponent<LineRenderer>();
            laserCollider = GetComponent<EdgeCollider2D>();
            laserCollider.edgeRadius = laser.widthMultiplier * colliderWidthMultiplier * 0.5f;

            laserTr = transform;
            laserTr.position = Vector3.zero;

            extendHandler = new UDEBulletManager.BulletMoveHandler(ExtendLaser);
        }

        /// <summary>
        /// Initializes the laser.
        /// </summary>
        /// <param name="origin">Origin of the local position of the laser in world space</param>
        /// <param name="originCharacter">Character who fired the laser</param>
        /// <param name="followOriginCharacter">If true, changes the origin of the laser to origin character's position</param>
        /// <param name="initialLocalHeadLocation">Start position of the laser in local space</param>
        /// <param name="timeScale">Time scale to use</param>
        protected virtual void Initialize(Vector2 origin,
                                          UDEBaseCharacter originCharacter,
                                          bool followOriginCharacter,
                                          Vector2 initialLocalHeadLocation,
                                          UDETime.TimeScale timeScale)
        {
            laserTr.position = origin;
            this.origin = origin;
            this.originCharacter = originCharacter;
            this.followOriginCharacter = followOriginCharacter;

            laserHead = new GameObject("Laser Head");
            laserHead.transform.parent = transform;
            laserHead.transform.localPosition = initialLocalHeadLocation;

            points = new List<Vector2>();
            points.Add(initialLocalHeadLocation);
            laser.positionCount = 1;
            laser.SetPosition(0, points[0]);
            laserCollider.points = points.ToArray();

            UDEBulletManager.Instance.MoveBullets += extendHandler;

            this.timeScale = timeScale;
            time = 0;
        }

        /// <summary>
        /// Destroies the laser.
        /// </summary>
        public virtual void DestroyLaser()
        {
            UDEBulletManager.Instance.MoveBullets -= extendHandler;
            Destroy(gameObject);
        }

        /// <summary>
        /// Enables and disables the collider of the laser.
        /// </summary>
        /// <param name="enabled">Whether the collider will be enabled</param>
        public void SetColliderEnabled(bool enabled)
        {
            laserCollider.enabled = enabled;
        }

        /// <summary>
        /// Sets the width of the laser.
        /// </summary>
        /// <param name="width">Width of the laser</param>
        public void SetLaserWidth(float width)
        {
            laser.widthMultiplier = width;
        }

        /// <summary>
        /// Only used internally in <see cref="UDEBulletManager.FixedUpdate"/>. Update the laser.
        /// </summary>
        /// <param name="deltaTime">Delta time from the last call</param>
        protected virtual void ExtendLaser(float deltaTime)
        {
            if (followOriginCharacter)
                laserTr.position = originCharacter.transform.position;

            laserCollider.edgeRadius = laser.widthMultiplier * colliderWidthMultiplier * 0.5f;
        }
    }
}