﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Util;
using SansyHuman.UDE.Util.Math;
using SansyHuman.UDE.Util.Builder;

public class TestBossPattern : UDEBaseShotPattern
{
    [SerializeField]
    private int NumberOfBullets = 10;
    [SerializeField]
    private float BulletSpeed = 2f;
    [SerializeField] UDECurveLaser laser;

    protected override IEnumerator ShotPattern()
    {
        yield return new WaitForSeconds(1f);

        UDECurveLaser newLaser = Instantiate<UDECurveLaser>(laser);
        UDEMath.TimeFunction easeFunc = UDETransitionHelper.CombineInOutEase(UDETransitionHelper.easeOutQuad, UDETransitionHelper.easeInQuad);
        UDEMath.CartesianTimeFunction laserPath = UDECurve.GetCurveByName("First").GetFunctionOfCurve().Composite(UDETransitionHelper.easeLinear).Composite(t => 0.5f * t);
        newLaser.Initialize(originEnemy.transform.position, originEnemy, false, Vector2.zero, UDETime.TimeScale.ENEMY, 2f, 2f, laserPath);

        WaitForSeconds interval = new WaitForSeconds(0.07f);
        float AngleDeg = (360f / NumberOfBullets);
        float AngleRef = 0f;
        float RefOmega = 0f;
        while (true)
        {
            for (int i = 0; i < NumberOfBullets; i++)
            {
                UDEBaseBullet bullet = UDEBulletPool.Instance.GetBullet(baseBullets[0]);
                UDEBulletMovement movement = UDEPolarMovementBuilder.Create().RadialSpeed(BulletSpeed).InitialAngle(i * AngleDeg + AngleRef).Build();
                Vector2 origin = originEnemy.transform.position;
                var formLocTuple = UDEMath.Polar2Cartesian(0.7f, movement.angle);
                Vector2 formLocation = new Vector2(formLocTuple.x, formLocTuple.y) + origin;
                bullet.Initialize(formLocation, origin, 0, originEnemy, this, true, movement);
            }
            AngleRef += RefOmega;
            RefOmega += 0.2f;
            if (RefOmega >= 360)
                RefOmega -= 360;
            if (AngleRef >= 360)
                AngleRef -= 360;
            // yield return interval;
            yield return StartCoroutine(UDETime.Instance.WaitForScaledSeconds(0.07f, UDETime.TimeScale.ENEMY));
        }
    }
}
