﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Management;

public class TestBossPattern2 : UDEBaseShotPattern
{
    [SerializeField]
    private float bulletSpeed = 2f;

    protected override IEnumerator ShotPattern()
    {
        WaitForSeconds interval = new WaitForSeconds(0.04f);

        yield return new WaitForSeconds(1f);
        while(true)
        {
            for(int i = 0; i < 2; i++)
            {
                UDEBaseBullet bullet = UDEBulletPool.Instance.GetBullet(baseBullets[0]);
                UDEBulletMovement movement1 = UDEBulletMovement.GetPolarMovement(0, bulletSpeed, 30, 0, 0);
                movement1.angle = Random.Range(0f, 360f);
                movement1.hasEndTime = true; movement1.endTime = 2.5f;
                UDEBulletMovement movement2 = UDEBulletMovement.GetPolarMovement(4f, -bulletSpeed, 30, 0, 0);
                Vector2 origin = originEnemy.transform.position;
                bullet.Initialize(origin, origin, 0, originEnemy, this, true, movement1, movement2);
                shottedBullets.Add(bullet);
            }
            yield return interval;
        }
    }

    private void RemoveReturnedBullets()
    {
        for (int i = 0; i < shottedBullets.Count; i++)
        {
            if (shottedBullets[i].Phase == 1 && shottedBullets[i].SqrMagnitudeFromOrigin < Mathf.Sqrt(0.5f))
                UDEBulletPool.Instance.ReleaseBullet(shottedBullets[i].gameObject);
        }
    }
}
