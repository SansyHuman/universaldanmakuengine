﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Pattern;
using SansyHuman.UDE.Object;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static GameObject player;

    public List<UDEBaseStagePattern> stages;
    public UDEEnemy enemy;
    public void Awake()
    {
        if(instance == null)
            GameManager.instance = this;

        Physics2D.IgnoreLayerCollision(8, 10);
        Physics2D.IgnoreLayerCollision(9, 11);

        GameManager.player = GameObject.FindGameObjectWithTag("Player");
        GameManager.player.transform.position = new Vector2(0, -4);
        // Invoke("Load", 5);
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartCorou());
    }

    void Load()
    {
        SceneManager.LoadScene("Test");
    }

    IEnumerator StartCorou()
    {
        yield return new WaitForSeconds(3f);
        stages[0].StartStage();
    }
}
