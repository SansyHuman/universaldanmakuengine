﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Util;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Util.Builder;

public class TestPlayer : UDEPlayer
{
    public UDEBaseBullet bullet;

    public override IEnumerator ShotBullet()
    {
        UDEBulletMovement move1 = UDECartesianMovementBuilder.Create().Speed(10).Angle(90).TangentialAccel(-10).Build();
        UDEBulletMovement move2_1 = UDECartesianMovementBuilder.Create().Speed(10).Angle(45).TangentialAccel(-10).NormalAccel(-30).Build();
        UDEBulletMovement move3_1 = UDECartesianMovementBuilder.Create().Speed(10).Angle(135).TangentialAccel(-10).NormalAccel(30).Build();
        UDEBulletMovement move2_2 = UDECartesianMovementBuilder.Create().Speed(10).Angle(67).TangentialAccel(-10).NormalAccel(-50).Build();
        UDEBulletMovement move3_2 = UDECartesianMovementBuilder.Create().Speed(10).Angle(113).TangentialAccel(-10).NormalAccel(50).Build();

        UDEBulletPool bulletPool = UDEBulletPool.Instance;
        Transform tr = gameObject.transform;

        while (true)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                UDEHomingBullet bullet1 = (UDEHomingBullet)bulletPool.GetBullet(bullet.gameObject);

                bullet1.Initialize(tr.position, this, move1);

                UDEHomingBullet bullet2 = (UDEHomingBullet)bulletPool.GetBullet(bullet.gameObject);
                UDEHomingBullet bullet3 = (UDEHomingBullet)bulletPool.GetBullet(bullet.gameObject);
                if (Input.GetKey(KeyCode.LeftShift))
                {

                    bullet2.Initialize(tr.position, this, move2_2);
                    bullet3.Initialize(tr.position, this, move3_2);
                }
                else
                {

                    bullet2.Initialize(tr.position, this, move2_1);
                    bullet3.Initialize(tr.position, this, move3_1);
                }
            }
            yield return StartCoroutine(UDETime.Instance.WaitForScaledSeconds(BulletFireInterval, UDETime.TimeScale.PLAYER));
        }
    }
}
