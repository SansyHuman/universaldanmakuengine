﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SansyHuman.UDE.Management;
using SansyHuman.UDE.Util;
using SansyHuman.UDE.Object;
using SansyHuman.UDE.Util.Builder;

public class PlayerMovement : MonoBehaviour
{
    private Transform tr;
    private WaitForSeconds shootInterval = new WaitForSeconds(0.05f);
    public GameObject Bullet;
    public float MoveSpeed = 5f;
    public bool Shootable = true;

    private UDEBulletPool bulletPool;
    private UDEBulletManager bulletManager;

    // Start is called before the first frame update
    void Start()
    {
        bulletPool = UDEBulletPool.Instance;
        bulletManager = UDEBulletManager.Instance;
        tr = GetComponent<Transform>();
        StartCoroutine("ShootBullet");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Move();
    }

    IEnumerator ShootBullet()
    {
        UDEBulletMovement move1 = UDECartesianMovementBuilder.Create().Speed(14).Angle(90).TangentialAccel(-10).Build();
        UDEBulletMovement move2_1 = UDECartesianMovementBuilder.Create().Speed(14).Angle(88).TangentialAccel(-10).NormalAccel(-30).Build();
        UDEBulletMovement move3_1 = UDECartesianMovementBuilder.Create().Speed(14).Angle(92).TangentialAccel(-10).NormalAccel(30).Build();
        UDEBulletMovement move2_2 = UDECartesianMovementBuilder.Create().Speed(14).Angle(80).TangentialAccel(-10).NormalAccel(-50).Build();
        UDEBulletMovement move3_2 = UDECartesianMovementBuilder.Create().Speed(14).Angle(100).TangentialAccel(-10).NormalAccel(50).Build();

        while (true)
        {
            if (Input.GetKey(KeyCode.Z) && Shootable)
            {
                UDEBaseBullet bullet1 = bulletPool.GetBullet(Bullet);
                
                bullet1.Initialize(tr.position, tr.position, 0, null, null, false, move1);

                UDEBaseBullet bullet2 = bulletPool.GetBullet(Bullet);
                UDEBaseBullet bullet3 = bulletPool.GetBullet(Bullet);
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    
                    bullet2.Initialize(tr.position, tr.position, 0, null, null, false, move2_1);
                    bullet3.Initialize(tr.position, tr.position, 0, null, null, false, move3_1);
                }
                else
                {
                    
                    bullet2.Initialize(tr.position, tr.position, 0, null, null, false, move2_2);
                    bullet3.Initialize(tr.position, tr.position, 0, null, null, false, move3_2);
                }
            }
            yield return shootInterval;
        }
    }

    private void Move()
    {
        float RealSpeed = MoveSpeed;
        if(Input.GetKey(KeyCode.LeftShift))
        {
            RealSpeed *= 0.5f;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            gameObject.transform.Translate(Vector3.right * RealSpeed * Time.deltaTime);
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                gameObject.transform.Translate(Vector3.left * RealSpeed * Time.deltaTime);
            }
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            gameObject.transform.Translate(Vector3.up * RealSpeed * Time.deltaTime);
        }
        else
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                gameObject.transform.Translate(Vector3.down * RealSpeed * Time.deltaTime);
            }
        }

        Vector3 pos = Camera.main.WorldToViewportPoint(gameObject.transform.position);
        if (pos.x < 0)
            pos.x = 0;
        if (pos.x > 1)
            pos.x = 1;
        if (pos.y < 0)
            pos.y = 0;
        if (pos.y > 1)
            pos.y = 1;
        gameObject.transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Collided with" + collision.tag);
    }
}
