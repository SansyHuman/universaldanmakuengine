﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletMovement : MonoBehaviour
{
    private Rigidbody2D rb2D;
    private Transform tr;
    public float BulletSpeed = 14f;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = gameObject.GetComponent<Rigidbody2D>();
        tr = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        float angle = (tr.eulerAngles.z + 90) * Mathf.Deg2Rad;
        rb2D.velocity = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * BulletSpeed;
    }
}
