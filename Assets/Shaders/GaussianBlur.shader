﻿Shader "SansyHuman/GaussianBlur"{
    Properties{
        _Color ("Tint", Color) = (0, 0, 0, 1)
        [HideInInspector]_MainTex ("Texture", 2D) = "white" {}
        _BlurSize ("Blur Size", Range(0, 1)) = 0
        _Samples ("Sample amount", Range(1, 100)) = 10
        _StandardDeviation ("Standard Deviation", Range(0, 1)) = 0.02
    }
    SubShader{
        Tags{
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
        }
        
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite off
        Cull off
        ZTest Always

        Pass{

            CGPROGRAM

            #include "UnityCG.cginc"

            #define PI 3.14159265359
            #define E 2.71828182846

            #pragma vertex vert
            #pragma fragment frag

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            float _BlurSize;
            int _Samples;
            float _StandardDeviation;

            struct appdata{
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f{
                float4 position : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            v2f vert(appdata v){
                v2f o;
                o.position = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET{
                fixed4 col = 0;
                float sum = 0;
                for(float x = 0; x < _Samples; x++){
                    for(float y = 0; y < _Samples; y++){
                        float xOffset = (x / _Samples - 0.5) * _BlurSize;
                        float yOffset = (y / _Samples - 0.5) * _BlurSize;
                        float2 uv = i.uv + float2(xOffset, yOffset);

                        float stDevSquared = _StandardDeviation * _StandardDeviation;
                        float gauss = (1 / sqrt(2 * PI * stDevSquared)) * pow(E, -((xOffset * yOffset) / (2 * stDevSquared)));
                        sum += gauss;
                        col += tex2D(_MainTex, uv) * gauss;
                    }
                }
                col /= sum;
                col *= _Color;
                return col;
            }

            ENDCG
        }
    }
}
